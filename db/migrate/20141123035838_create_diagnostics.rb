class CreateDiagnostics < ActiveRecord::Migration
  def change
    create_table :diagnostics do |t|
      t.float :diagnostic_code
      t.string :diagnostic_name

      t.timestamps
    end
  end
end
