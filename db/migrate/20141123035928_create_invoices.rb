class CreateInvoices < ActiveRecord::Migration
  def change
    create_table :invoices do |t|
      t.float :invoice_total
      t.integer :diagnostic_id

      t.timestamps
    end
  end
end
