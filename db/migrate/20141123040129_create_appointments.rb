class CreateAppointments < ActiveRecord::Migration
  def change
    create_table :appointments do |t|
      t.integer :physician_id
      t.integer :patient_id
      t.integer :invoice_id
      t.integer :time_slot_id
      t.date :appointment_date
      t.string :note

      t.timestamps
    end
  end
end
