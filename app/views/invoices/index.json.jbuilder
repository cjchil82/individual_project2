json.array!(@invoices) do |invoice|
  json.extract! invoice, :id, :invoice_total, :diagnostic_id
  json.url invoice_url(invoice, format: :json)
end
