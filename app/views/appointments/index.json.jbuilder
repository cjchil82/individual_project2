json.array!(@appointments) do |appointment|
  json.extract! appointment, :id, :physician_id, :patient_id, :invoice_id, :time_slot_id, :appointment_date, :note
  json.url appointment_url(appointment, format: :json)
end
