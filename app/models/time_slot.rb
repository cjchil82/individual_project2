class TimeSlot < ActiveRecord::Base
  has_many :appointments
  has_many :patients, :through => :patient_time_slots
end
