class Appointment < ActiveRecord::Base
  belongs_to :physician
  belongs_to :patient
  belongs_to :invoice
  belongs_to :time_slot

  #def valid?
  #  taken = where(appointment_date: appointment_date, time_slot_id: time_slot_id)
  #  save unless taken
  #end

end
